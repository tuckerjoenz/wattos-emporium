import React from "react";
import "./Spaceships.css";

const Spaceships = props => {
  return (
    <div className="spaceship-container">
      <div className="spaceship-pic">
        <img src={props.imgUri} alt="" />
      </div>
      <div className="spaceship-info">
        <div>
          <span className="bold">Name:</span> {props.name}
        </div>
        <div>
          <span className="bold">Manufacturer:</span> {props.manufacturer}
        </div>
        <div>
          <span className="bold">Class:</span> {props.classOf}
        </div>
        <div>
          <span className="bold">Price:</span> {props.price}
        </div>

        <div className="description">Tech Specs</div>

        <div className="techspecs">
          <div>
            <span className="bold">Length:</span> {props.techspecs.length}
          </div>
          <div>
            <span className="bold">Max Accel:</span> {props.techspecs.maxaccel}
          </div>
          <div>
            <span className="bold">MGLT:</span> {props.techspecs.MGLT}
          </div>
          <div>
            <span className="bold">Max Atmospheric Speed:</span>{" "}
            {props.techspecs.maxatmosphericspeed}
          </div>
          <div>
            <span className="bold">Shielding:</span> {props.techspecs.shielding}
          </div>
          <div>
            <span className="bold">Hull:</span> {props.techspecs.hull}
          </div>
          <div>
            <span className="bold">Armament:</span> {props.techspecs.armament}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Spaceships;
