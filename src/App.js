import React, { Component } from "react";
import Spaceships from "./Spaceships/Spaceships";
import "./App.css";
import { DB_CONFIG } from "./Config/config";
import firebase from "firebase/app";
import "firebase/database";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.secondary
  }
});

class App extends Component {
  constructor(props) {
    super(props);

    if (!firebase.apps.length) {
      this.app = firebase.initializeApp(DB_CONFIG);
    }

    this.db = this.app
      .database()
      .ref()
      .child("products");

    this.state = {
      spaceships: []
    };
  }

  componentWillMount() {
    const spaceship = this.state.spaceships;
    console.log(spaceship);

    // DataSnapShot
    this.db.on("child_added", snap => {
      spaceship.push({
        id: snap.key,
        classOf: snap.val().classOf,
        img: snap.val().img,
        manufacturer: snap.val().manufacturer,
        name: snap.val().name,
        techspecs: snap.val().techspecs
      });

      this.setState({
        spaceships: spaceship
      });
    });
  }

  render() {
    return (
      <div>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Watto's Space Emporium
            </Typography>
          </Toolbar>
        </AppBar>
        <div className="content">
          <Grid container spacing={24}>
            {this.state.spaceships.map(spaceship => {
              return (
                <Grid item xs={12} sm={3} className="spaceship-desc">
                  <Spaceships
                    name={spaceship.name}
                    imgUri={spaceship.img}
                    manufacturer={spaceship.manufacturer}
                    classOf={spaceship.classOf}
                    price={spaceship.price}
                    techspecs={spaceship.techspecs}
                  />
                </Grid>
              );
            })}
          </Grid>
        </div>
      </div>
    );
  }
}

export default App;
